package com.te.springmongoquery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringmongoqueryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringmongoqueryApplication.class, args);
	}

}
