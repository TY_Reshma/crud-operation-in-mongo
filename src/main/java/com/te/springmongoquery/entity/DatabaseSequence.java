package com.te.springmongoquery.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "Database_Sequence")
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatabaseSequence {
	
	@Id
	private String id;
	private int seqNo;
}
