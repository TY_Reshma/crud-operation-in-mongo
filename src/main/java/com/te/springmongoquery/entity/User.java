package com.te.springmongoquery.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "User")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
	
	@Transient
	public static final String SEQUENCE_NAME = "users_sequence";
	
	@Id
	private Integer id;
	private String name;
	private Integer age;

	private Address address;
}
