package com.te.springmongoquery.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.te.springmongoquery.entity.User;

@Repository
public interface UserRepository extends MongoRepository<User, Integer> {

	@Query("{name : ?0}")
	List<User> findByNameByQuery(String name);
	
	 
}
