package com.te.springmongoquery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.te.springmongoquery.entity.User;
import com.te.springmongoquery.repository.UserRepository;

@Service
public class ServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired
	private SequenceGeneratorService generatorService;

	@Override
	public User addUser(User user) {
		user.setId(generatorService.sequenceNumber(User.SEQUENCE_NAME));
		User save = userRepository.save(user);
		return save;
	}

	@Override
	public User findByNameByQuery(String name) {
		List<User> findByNameByQuery = userRepository.findByNameByQuery(name);
		return findByNameByQuery.get(0);
	}

	@Override
	public List<User> findByNameStartWith(String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").regex("^R"));
		return mongoTemplate.find(query, User.class);
	}

	@Override
	public List<User> ageBetween20To25(int ltAge, int gtAge) {
       Query query = new Query();
       query.addCriteria(Criteria.where("age").lt(20).gt(25));
		return mongoTemplate.find(query, User.class);
	}

	@Override
	public List<User> sortAge() {
		Query query = new Query();
		query.with(Sort.by(Sort.Direction.ASC, "age"));
		return mongoTemplate.find(query, User.class);
	}

}
