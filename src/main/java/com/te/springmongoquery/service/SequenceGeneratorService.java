package com.te.springmongoquery.service;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.te.springmongoquery.entity.DatabaseSequence;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class SequenceGeneratorService {

	@Autowired
	private MongoOperations mongoOperations;
	
	
	public int sequenceNumber(String seqName) {
		//get sequence number
		Query query = new Query(Criteria.where("id").is(seqName));
		
		//update
		Update update = new Update().inc("seqNo", 1);
		
		//modify the document
		DatabaseSequence counter = mongoOperations.findAndModify(query, update, options().returnNew(true).upsert(true),DatabaseSequence.class);
		
		return !Objects.isNull(counter)?counter.getSeqNo():1;
		
	}
	
}
