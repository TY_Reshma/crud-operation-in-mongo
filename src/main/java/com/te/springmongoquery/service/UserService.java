package com.te.springmongoquery.service;

import java.util.List;

import com.te.springmongoquery.entity.User;

public interface UserService {
	public User addUser(User user);

	public User findByNameByQuery(String name);
	
	public List<User> findByNameStartWith(String name);
	
	public List<User> ageBetween20To25(int ltAge,int gtAge);
	
	public List<User> sortAge();
	
	
	
	
	
	
	
	
	
	
	
	
	
}
