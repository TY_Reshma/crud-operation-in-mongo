package com.te.springmongoquery.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.te.springmongoquery.entity.User;
import com.te.springmongoquery.service.UserService;

@RestController
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping("/add")
	public User addUser(@RequestBody User user) {
		User addUser = userService.addUser(user);
		return addUser;
	}

	@GetMapping("/get")
	public User findByNameByQuery(@RequestParam String name) {
		User findByNameByQuery = userService.findByNameByQuery(name);
		return findByNameByQuery;
	}

	@GetMapping("/findByNameStartWith")
	public List<User> findByNameStartWith(@RequestParam String name) {
		List<User> findByNameStartWith = userService.findByNameStartWith(name);
		return findByNameStartWith;
	}

	@GetMapping("/ageBetween20To25/{age1}/{age2}")
	public List<User> ageBetween20To25(@PathVariable int age1,@PathVariable int age2) {
           List<User> ageBetween20To25 = userService.ageBetween20To25(age1, age2);
		return ageBetween20To25;
	}

	@GetMapping("/sortByAge")
	public List<User> sortAge(){
		List<User> sortAge = userService.sortAge();
		return sortAge;
		
	}
	
	
	
	
	
	
}
